<!DOCTYPE html>
<html>
<head>
	<title>Premier Dead Sea</title>
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div class="wrapper">
		<section id="section1">
			<!-- <div style="text-align: center" class="top_ttl"> -->
			<div class="top_ttl">
				<h1>הזדמנות נדירה</h1>
				<p class="subttl">לבעלי עסקים באוסטרליה</p>
			</div>
			<div class="product">
				<img src="images\section1_product.png">
			</div>
			<p>Premier מותג הקוסמטיקה היוקרתי</p>
			<p>!מחפש מספר מוגבל של זכיינים לנבחרת המנצחת</p>
			<p>למצטרפים כעת - הטבה אחת לבחירה</p>
			<div>
				<div>
					<p>$20,000 קרדיט</p>
				</div>
				<div>
					<p>או</p>
				</div>
				<div>
					<p>הזמנת מלאי !ראשונה חינם</p>
				</div>
			</div>
			<div class="star_list">
				<div>
					<p>מתחייבים בעזרה במציאת לוקיישנים מובילים</p>
				</div>
				<div class="star_sep"></div>
				<div>
					<p>מתחייבים בעזרה במציאת לוקיישנים מובילים</p>
				</div>
				<div class="star_sep"></div>
				<div>
					<p>מתחייבים בעזרה במציאת לוקיישנים מובילים</p>
				</div>
			</div>
			<div class="shop">
				<p class="shop_ttl">We are Premier</p>
				<img src="images\section3_bg.jpg">
			</div>
			<div class="star_list">
				<div>
					<p>מתחייבים בעזרה במציאת לוקיישנים מובילים</p>
				</div>
				<div class="star_sep"></div>
				<div>
					<p>מתחייבים בעזרה במציאת לוקיישנים מובילים</p>
				</div>
				<div class="star_sep"></div>
				<div>
					<p>מתחייבים בעזרה במציאת לוקיישנים מובילים</p>
				</div>
			</div>
		</section>
		<section id="section2">
			Text
		</section>
		<section id="section3">
			
		</section>
		<footer>
			
		</footer>
	</div>
</body>
</html>